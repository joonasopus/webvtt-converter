import React from "react";
import * as webvtt from 'node-webvtt';
import pathParse from 'path-parse';

const Main = () => {
    const makeTextFile = (txt) => {
        const data = new Blob([txt], {type: 'text/plain'});
        return window.URL.createObjectURL(data);
    };

    const selectFile = (event) => {
        let files = event.target.files;
        let reader = new FileReader();
        let txt = '';

        reader.onloadend = (r) => {
            const subtitles = r.target.result
            const parsed = webvtt.parse(subtitles);

            parsed.cues.map((cue) => {
                return txt += ` ${cue.text}`;
            });

            txt = txt.substr(1);

            makeTextFile(txt);

            const link = document.getElementById('downloadlink');
            link.href = makeTextFile(txt);
            link.style.display = 'block';
            link.download = pathParse(files[0].name).name;

        };

        reader.readAsText(files[0]);
    };

    return (
        <div>
            <label className="btn btn-default">
                <input type="file" onChange={selectFile} accept={'.vtt'}/>
            </label>
            <div className="card">
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="downloadlink" style={{display: 'none'}}>Download</a>
            </div>
        </div>
    );
};

export default Main;
