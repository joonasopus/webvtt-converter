Application for converting .vtt files to .txt.

## Installation

```
$ docker build -t webvttconverter .
$ docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 3000:3000 -e CHOKIDAR_USEPOLLING=true webvttconverter
$ go to http://localhost:3000/
```
